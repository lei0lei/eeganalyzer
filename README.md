# EEGAnalyzer

## Description
本python包用于进行心脏EEG信号的分析，首要任务是进行心梗检测。

## Installation
本python包包含两个版本，dev版本以及普通版本，dev版本下可以进行模型的修改和训练，普通版本下只能进行模型的部署和整体算法测试,信号的基础处理功能。
```sh
pip install eeganalyzer[dev]
```

```sh
pip install eeganalyzer
```
## Quick Usage


## Test and Deploy

## Models
### generator

### analyzer

### feature extractor


## Badges

## Usage
### Frome template

### Develop new models

### just-in-time


## Roadmap
### dataset
- [ ] PTB-xl
- [ ] CPSC2018
### models
- [ ] CNN
### device
- [ ] cpu
- [ ] gpu
### visualizer
- [ ] Tui

## Contributing
not supposrted

## Authors and acknowledgment
lei.lei.fan.meng@gmail.com

## License
For open source projects, say how it is licensed.

## Project status
