data_path = 'F:\\11k'
patient_id=0000
segment_id=48
start=4000
length=10240
import wfdb
filename = f'{data_path}\\p0{str(patient_id)[:1]}\\p{patient_id:05d}\\p{patient_id:05d}_s{segment_id:02d}'

rec = wfdb.rdrecord(filename, sampfrom=start, sampto=start+length)
ann = wfdb.rdann(filename, "atr", sampfrom=start, sampto=start+length, shift_samps=True)
print(ann.symbol)
wfdb.plot_wfdb(rec, ann, plot_sym=True, figsize=(15,4))