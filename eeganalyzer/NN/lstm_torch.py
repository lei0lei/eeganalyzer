from signals import wfdb2eeg
data_path = 'F:\\11k'
patient_id=0000
segment_id=00
filename = f'{data_path}\\p0{str(patient_id)[:1]}\\p{patient_id:05d}\\p{patient_id:05d}_s{segment_id:02d}'

trans_internal = wfdb2eeg.Wfdb2Eeg()

eegsignal = trans_internal.data2eeg(filename)
data=eegsignal.frame.channels.to_pandas()
data=data.reset_index()
data.insert(0,'time',range(0,len(data)))
data.insert(0,'id',[0]*len(data))

# imports for training
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import EarlyStopping, LearningRateMonitor
# import dataset, network to train and metric to optimize
from pytorch_forecasting import TimeSeriesDataSet, TemporalFusionTransformer, QuantileLoss,PoissonLoss


max_encoder_length = 1000
max_prediction_length = 500

training = TimeSeriesDataSet(
    data,
    time_idx= 'time',  # column name of time of observation
    target= 'ecg',  # column name of target to predict
    group_ids=[ 'id' ],  # column name(s) for timeseries IDs
    max_encoder_length=max_encoder_length,  # how much history to use
    max_prediction_length=max_prediction_length, )


validation = TimeSeriesDataSet.from_dataset(training, data, min_prediction_idx=training.index.time.max() + 1, stop_randomization=True)

# convert datasets to dataloaders for training
batch_size = 16
train_dataloader = training.to_dataloader(train=True, batch_size=batch_size, num_workers=2)
val_dataloader = validation.to_dataloader(train=False, batch_size=batch_size, num_workers=2)

# create PyTorch Lighning Trainer with early stopping
early_stop_callback = EarlyStopping(monitor="val_loss", min_delta=1e-4, patience=1, verbose=False, mode="min")
lr_logger = LearningRateMonitor()
trainer = pl.Trainer(
    max_epochs=100,
    gpus=0,  # run on CPU, if on multiple GPUs, use accelerator="ddp"
    gradient_clip_val=0.1,
    limit_train_batches=30,  # 30 batches per epoch
    callbacks=[lr_logger],#, early_stop_callback],
    logger=TensorBoardLogger("lightning_logs")
)

# define network to train - the architecture is mostly inferred from the dataset, so that only a few hyperparameters have to be set by the user
tft = TemporalFusionTransformer.from_dataset(
    # dataset
    training,
    # architecture hyperparameters
    hidden_size=500,
    attention_head_size=1,
    dropout=0.01,
    hidden_continuous_size=16,
    # loss metric to optimize
    loss=PoissonLoss(),
    # logging frequency
    log_interval=0,
    # optimizer parameters
    learning_rate=0.0003,
    reduce_on_plateau_patience=4
)


print(f"Number of parameters in network: {tft.size()/1e3:.1f}k")

# find the optimal learning rate
res = trainer.lr_find(
    tft, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader, early_stop_threshold=1000.0, max_lr=0.3,
)
# and plot the result - always visually confirm that the suggested learning rate makes sense
print(f"suggested learning rate: {res.suggestion()}")
fig = res.plot(show=True, suggest=True)
fig.show()

# fit the model on the data - redefine the model with the correct learning rate if necessary
trainer.fit(
    tft, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader,
)













