'''Ecg raw dataclass defination.
    Using this module to transfer other EEG data to internal formats.
    This internal format use xarray as a backend.
'''
import  abc
from attrs import define,field,frozen
import numpy as np
# from detectors import QRS, ST, Rpeak
from xarray import DataArray
from xarray import Dataset as _Dataset
import datetime

@frozen
class EegChannel():
    '''A channel of eeg singal, using xarray data structure.
    '''
    record_name:       list[str] = field(kw_only=True)
    # channels is all the data channel and channel name of record（s)
    channels:          _Dataset = field(kw_only=True)
    # channel_num is the num od data channel
    channel_num:       list[int] = field(kw_only=True)
    # point num of a channel
    samples:           list[int] = field(kw_only=True)
    sampling_rate:     list[float] = field(kw_only=True)
    base_counter:      list[float] = field(kw_only=True)
    base_time:         list[datetime.time] = field(kw_only=True)
    base_date:         list[datetime.time] = field(kw_only=True)
    adc_res:           list[int] = field(kw_only=True)
    check_sum:         list[int] = field(kw_only=True)
    fmt:               list[str] = field(kw_only=True)

@define
class ChannelAux():
    '''Place anno info or aux info from algos
    '''
    point_aux:        _Dataset = field( default=_Dataset(dict(info='no point aux infos error')))
    point_aux_name:   dict = field(default={})
    point_anno:       _Dataset = field( default=_Dataset(dict(info='no point anno infos error')))
    point_anno_name:  _Dataset = field( default=_Dataset(dict(info='no point anno infos error')))
    anno_describ:     str = field(default='annotion meaning')

@define
class StreamEegSignal():
    '''Class for single stream eeg signal.
    
    '''
    pass


@define
class RecordEegSignal():
    '''Eeg record class,
    this is a container class.
    '''
    frame: EegChannel
    frame_anno: ChannelAux

    def slice(self):
        raise NotImplementedError

    def detect_R(self):
        raise NotImplementedError

    def clear_aux(self):
        raise NotImplementedError

    def clear_anno(self):
        raise NotImplementedError

    def visualize(self):
        raise NotImplementedError


    def visualize_with_anno(self):
        raise NotImplementedError

    def visualize_with_aux(self):
        raise NotImplementedError






class EegSignalInterface(metaclass=abc.ABCMeta):
    '''Interface for transforming any signal to internal Eeg signal.
    '''
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'data2eeg') and 
                callable(subclass.data2eeg) and 
                hasattr(subclass, 'eeg2data') and
                callable(subclass.eeg2data) or 
                NotImplemented)
    
    @abc.abstractclassmethod
    def data2eeg(self, source_path: str) -> RecordEegSignal:
        '''Transform random data to internal eeg.
        '''
        raise NotImplementedError

    @abc.abstractclassmethod
    def eeg2data(self, target_path: str) -> str:
        '''Transform eeg to external data.
        '''
        raise NotImplementedError
    