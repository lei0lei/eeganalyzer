'''This is a example of Icentia11k dataset to internal data structure
'''
import sys
sys.path.append('F:\\polar\\eeganalyzer')
from eeganalyzer.signals.eeg_raw import EegSignalInterface, RecordEegSignal, StreamEegSignal
from eeganalyzer.signals.eeg_raw import EegChannel, StreamEegSignal, RecordEegSignal, ChannelAux

import wfdb
from wfdb import  Record, MultiRecord
import xarray as xr
import numpy as np
import pandas as pd

@EegSignalInterface.register
class Wfdb2Eeg:
    '''Transform wfdb data to internal eeg data.
    '''
    
    def data2eeg(self, source_path: str) -> RecordEegSignal:
        '''Transform random data to internal eeg.
        '''
        rec = wfdb.rdrecord(source_path)
        # Some dataset support using header file to read data and return Record object.
        # hea = wfdb.rdheader(source_path)

        # Numpy array of all records
        record_data = rec.p_signal.transpose()
        # Signal name of all records
        record_name = rec.sig_name
        namedrecord= dict(zip(record_name, record_data))

        channelaux = ChannelAux()
        ann = wfdb.rdann(source_path, "atr")

        try:
            ann = wfdb.rdann(source_path, "atr")
            channelaux.point_anno = xr.Dataset(dict(zip(record_name,[ann.sample])))
            channelaux.point_anno_name = xr.Dataset(dict(zip(record_name,[ann.symbol])))
            channelaux.anno_describ = 'N: normal\n S: \n V: \n Q: \n'
        except FileNotFoundError:
            print(f'Your data has no available annotation or erro while importing anno')

        eegsignal = EegChannel(
                        record_name= [str(rec.record_name)],
                        channels = xr.Dataset(namedrecord),
                        channel_num = [int(rec.n_sig)],
                        samples = [int(rec.sig_len)],
                        sampling_rate = [int(rec.fs)],
                        base_counter = [0],
                        base_time = [rec.base_time],
                        base_date = [rec.base_date],
                        adc_res = [int(rec.adc_res[0])],
                        check_sum = [int(rec.checksum[0])],
                        fmt = [str(rec.fmt[0])],
        )

        _record_eeg_signal = RecordEegSignal(frame = eegsignal, frame_anno=channelaux)

        # _record_eeg_signal.frame_aux = channelaux


        return _record_eeg_signal
        # raise NotImplementedError
    
    def eeg2data(self, record: RecordEegSignal, target_path: str) -> str:
        '''Transform eeg to external data.
        '''
        # record = record

        # record_wfdb = 'path'
        # return record_wfdb
        raise NotImplementedError
    

if __name__ == '__main__':
    import sys
    sys.path.append('F:\\polar\\eeganalyzer\\eeganalyzer')
    data_path = 'F:\\11k'
    patient_id=0000
    segment_id=4
    start=4000
    length=10240
    filename = f'{data_path}\\p0{str(patient_id)[:1]}\\p{patient_id:05d}\\p{patient_id:05d}_s{segment_id:02d}'
    # filename = 'F:\\ptb\\00001_lr'
    print(f'test wfdb to eeg')
    trans_internal = Wfdb2Eeg()
    eegsignal = trans_internal.data2eeg(filename)
    # print(eegsignal.frame.channels.I)
    print(eegsignal.frame.channels['ecg'].to_numpy())
    print(f'-----------------')