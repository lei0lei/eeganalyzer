import sys
sys.path.append('F:\\polar\\eeganalyzer')

print(sys.path)

from eeganalyzer.datasets.dataset import EegDataset
import rich
import mne
from eeganalyzer.signals.wfdb2eeg import Wfdb2Eeg
import os
import xarray as xr
from rich.progress import track
import pandas as pd
import numpy as np


class IcentiaEcgDataset(EegDataset):
    '''Icentia11k Ecg Dataset loader
    source: https://physionet.org/content/icentia11k-continuous-ecg/1.0/
    description: 
    '''
    def __init__(self, source):
        
        self.source = source
    def __str__(self):

        return super().__descript()
    
    def __download(self, source):
        return super().__download(source)
    

    def visualizer(self):
        pass


    def __loader(self):
        return super().__loader()
    
    def generate_pid(self,persons = 0, persone=1):
        """collect all data for some person

        Args:
            persons (int, optional): _description_. Defaults to 0.
            persone (int, optional): _description_. Defaults to 1.
        """        
        parent_idl = [f'{i:05d}' for i in range(persons,persone)]
        for index in track(parent_idl):
            filepath = os.path.join(self.source, f'p{index[:2]}',f'p{index}')

            pdf = pd.DataFrame([],columns=['tt','points','annos'])
            trans_internal = Wfdb2Eeg()
            with open(os.path.join(filepath,'RECORDS'), 'r') as records:
                segments = records.readlines()
            idx = 0
            for segment in segments:
                try:
                    data = trans_internal.data2eeg(os.path.join(filepath, segment.split('\n')[0]))

                    points = data.frame.channels['ecg'].to_numpy()
                    annos = np.array(len(points)*['Q'])
                    tt = np.arange(idx, idx+len(points))
                    idx += len(points)

                    for i, k in enumerate(data.frame_anno.point_anno):
                        annos[k] = data.frame_anno.point_anno_name[i]

                    pdi = pd.DataFrame({'tt':tt,'points':points,'annos':annos})

                    pdf = pd.concat([pdf,pdi])
                    print(f'-----------')
                except:
                    pass
                # data = trans_internal.data2eeg(os.path.join(filepath, segment.split('\n')[0]))

                # points = data.frame.channels['ecg'].to_numpy()
                # annos = np.array(len(points)*['Q'])
                # tt = np.arange(idx, idx+len(points))
                # idx += len(points)
                # for i, k in enumerate(data.frame_anno.point_anno):
                #     annos[k] = data.frame_anno.point_anno_name[i]

                # pdi = pd.DataFrame({'tt':tt,'points':points,'annos':annos})

                # pdf = pd.concat([pdf,pdi])
                # print(f'-----------')
            print(pdf)    
        return pdf

    def generate_index(self, start=0, end=10999):
        """Generate an index file for icentia11k dataset

        Args:
            start (int, optional): start person index. Defaults to 0.
            end (int, optional): end person index. Defaults to 10999.
        
        Returns:
            list[dict]: all annos in icentia11k dataset
        """      
        parent_idl = [f'{i:05d}' for i in range(start,end)]
        anno_index = []
        trans_internal = Wfdb2Eeg()
        for index in track(parent_idl):
            filepath = os.path.join(self.source, f'p{index[:2]}',f'p{index}')
            
            with open(os.path.join(filepath,'RECORDS'), 'r') as records:
                segments = records.readlines()
            for segment in segments:
                try:
                    data = trans_internal.data2eeg(os.path.join(filepath, segment.split('\n')[0]))
                
                    anno = {}
                    anno['record'] = f'p{index}'
                    anno['segment'] = os.path.join(filepath, segment).split('\n')[0]
                    
                    if 'S' in data.frame_anno.point_anno_name['ecg'].values:
                        anno['annos'] = 'S'
                    elif 'V' in data.frame_anno.point_anno_name['ecg'].values:
                        anno['annos'] = 'V'
                    elif 'N' in data.frame_anno.point_anno_name['ecg'].values:
                        anno['annos'] = 'N'
                    else:
                        anno['annos'] = 'Q'

                    # print(f'----------------')
                    # print(anno)
                    anno_index.append(anno)
                except:
                    pass
        return anno_index
    

    def generate_segment(self, savepath='', start=0, end = 10999):
        parent_idl = [f'{i:05d}' for i in range(start,end)]
        anno_index = []
        trans_internal = Wfdb2Eeg()
        
        def find_indices(list_to_check, item_to_find):
            return [idx for idx, value in enumerate(list_to_check) if value == item_to_find]


        for index in track(parent_idl):
            filepath = os.path.join(self.source, f'p{index[:2]}',f'p{index}')
            try:
                with open(os.path.join(filepath,'RECORDS'), 'r') as records:
                    segments = records.readlines()

                for segment in segments:
                    
                    _tmp_anno = '' 
                    
                    data = trans_internal.data2eeg(os.path.join(filepath, segment.split('\n')[0]))
                    
                    anno = {}
                    anno['record'] = f'p{index}'
                    anno['segment'] = os.path.join(filepath, segment).split('\n')[0]
                    anno['annos'] = ''
                    anno['start'] = -1
                    anno['end'] = -1
                    _all_anno = data.frame_anno.point_anno_name['ecg'].values
                    _all_position =  data.frame_anno.point_anno['ecg'].values
                    # print(len(_all_anno))

                    _plus_position = find_indices(_all_anno, '+')
                    # print(_plus_position)
                    for k,p in enumerate(_plus_position[:-1]):
                        # print(p)
                        anno['start'] = _all_position[_plus_position[k]+1]
                        anno['end'] = _all_position[_plus_position[k+1]-1]
                        anno['annos'] = _all_anno[_plus_position[k]+1]
                        # print(f'------------------------')
                        # to debug
                        # print(_all_anno[_plus_position[k]+1:_plus_position[k+1]-1])
                        # print(anno['annos'])
                        # print(f'------------------------')
                        anno_index.append(anno.copy())
            except:
                pass
        return anno_index


if __name__ == '__main__':

    import csv
    dataset = IcentiaEcgDataset(source = 'I:\\icentia11k-single-lead-continuous-raw-electrocardiogram-dataset-1.0')


    # to_csv = dataset.generate_index(1000,2000)
    # keys = to_csv[0].keys()
    # with open('out2k.csv','w', newline='') as output_file:
    #     dict_writer = csv.DictWriter(output_file, keys)
    #     dict_writer.writeheader()
    #     dict_writer.writerows(to_csv)
    for i in range(100,11000,100):
        print(f' processing {i}/11000')
        to_csv = dataset.generate_segment('',i,i+100)
        keys = to_csv[0].keys()
        with open(f'sout{int(i/100)}h.csv','w', newline='') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(to_csv)

    # pdf = dataset.generate_pid(1,2)

    # pdf.to_csv('aa.csv')
    # from merlion.utils import TimeSeries

    # from merlion.models.defaults import DefaultDetectorConfig, DefaultDetector

    # model = DefaultDetector(DefaultDetectorConfig())

    # train_data = TimeSeries.from_pd(pdf)
    # model.train(train_data=train_data)
