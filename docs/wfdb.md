# wfdb格式

本文对wfdb(waveform-database)格式进行详细解释.

wfdb是record(数据和标注)的集合，每个record包含三个文件:
- header文件(txt), 一个record的name和description
- 信号文件(bin), 二进制文件包含一个或者多个数字信号
- 标注文件(bin), 包含标签的二进制文件，每个标签指向确定的样本，并描述对应时刻信号的特征。

1. 原始信号存储
    - 信号可能由不同采样率
    - 每个样本的信号可能由不同的bit
    - 一个record的信号可能存储在不同的格式下
    - 在非均匀间隔上进行的测量可能会被存储为标注

2. 事件定义与存储
    - 可能添加新的事件类型
    - 存在的事件类型可能进行更改来添加新的信息
    - 事件可能被固定到一个通道
    - 事件可能重叠
    - 事件可以被链接到其他事件


处理wfdb格式文件的[python包](https://wfdb.readthedocs.io/en/latest/index.html)

# Record




# anno